/* Javascript Document */
var express = require('express');
var mock = require('mockjs');
var process = require('process');
var app = express();

/**
 * 接口路由
 */
// 使用mockjs模拟的数据
app.use('/mapi', require('./routers/mockData'));
// 自定义的json数据
app.use('/lapi',require('./routers/localData'));

//解决请求时的跨域问题
app.all('*', function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "X-Requested-With");
  res.header("Access-Control-Allow-Methods", "PUT,POST,GET,DELETE,OPTIONS");
  res.header("X-Powered-By", ' 3.2.1')
  res.header("Content-Type", "application/json;charset=utf-8");
  next();
});

//设置端口号，默认是8080
var port = (function () {
  if (typeof (process.argv[2]) !== 'undefined') {
    if (isNaN(process.argv[2])) {
      throw '请确认端口号格式是否正确'
    } else {
      return process.argv[2]
    }
  } else {
    return 8080
  }
})()



app.listen(port, function () {
  console.info('LocalServer listening on port  ' + port);
})