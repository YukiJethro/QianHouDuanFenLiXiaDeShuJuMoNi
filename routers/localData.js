var express = require('express');
var fs = require('fs');
var router = express.Router();
var app = express();

//读取数据,自定义的json统一放置于db文件夹下
function getData(dbname){
  var result = fs.readFileSync('./db/'+dbname);
  var data = JSON.parse(result);
  return data
}

router.get('/', function (req, res) {
  res.send('lapi调用的是本地自定义的json数据');

})
router.get('/userlist', function (req, res) {
  var data = getData('db.json');
  res.send(data);
})
module.exports = router;