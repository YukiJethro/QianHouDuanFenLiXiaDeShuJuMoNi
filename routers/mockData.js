var express = require('express');
var mock = require('mockjs');
var router = express.Router();
var app = express();

router.get('/', function (req, res) {
  res.send('mapi调用的是mockjs生成的模拟数据');
})
router.get('/userlist', function (req, res) {
  res.json(mock.mock({
    "status": 200,
    "data|1-9": [{
      'name': '@name',
      'age|1-100': 100,
      'color': '@color'
    }]
  }));
})
module.exports = router;