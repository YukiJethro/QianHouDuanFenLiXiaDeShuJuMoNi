# 前后端分离下的数据模拟

#### 启动项目

- 克隆Demo到本地`git clone https://gitee.com/YukiJethro/QianHouDuanFenLiXiaDeShuJuMoNi.git`
- 进入Demo目录`cnpm install`安装必要的依赖
- 启动项目`node app.js 8888`监听8888端口

#### Demo结构

![demo目录结构](https://gitee.com/uploads/images/2017/1229/233926_bac75723_1300952.png "WX20171229-233910.png")
- db -------- 本地json文件存放目录
    - `db.json` -------- json数据文件
- routers -------- 路由文件，分为Mock数据和本地数据访问
    - `localData.js` -------- 访问本地自定义数据的路由
    - `mockData.js` -------- 访问Mockjs生成的数据的路由
- `app.js` -------- 入口文件
- `package.json` -------- 配置相关的文件

#### 路由规则

- `localhost:端口号/mapi/userlist` 通过这个接口，访问的是Mockjs模拟出来的数据

![通过mockjs模拟数据并返回](https://gitee.com/uploads/images/2017/1229/235329_02595205_1300952.png "WX20171229-235315.png")

- `localhost:端口号/lapi/userlist` 通过这个接口，访问的是本地自定义的json返回的数据

![通过自定义的json数据](https://gitee.com/uploads/images/2017/1229/235605_d8c2ee8d_1300952.png "WX20171229-235551.png")

#### Tips

- 自定义的json文件，存放于db文件夹下面
- 启动服务时，如果没有设置端口，服务默认的端口号是8080
- 代码还有待完善